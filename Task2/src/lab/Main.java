package lab;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws FileNotFoundException {
        Scanner txt = new Scanner(new File("text"));
        String myText = txt.nextLine();
        txt.close();
        int[] intLineToUp = new int[20];
        int[] intLineToDown = new int[20];
        int count = 0;
        for (String num : myText.split(",")) {
            intLineToUp[count] = Integer.parseInt(num);
            intLineToDown[count] = Integer.parseInt(num);
            count++;
        }

        toDown(intLineToDown);
        System.out.println();
        toUp(intLineToUp);

    }
    public static int [] toUp (int[]intLine){
        System.out.println("По возрастанию: ");
        for (int count = intLine.length - 1; count > 0; count--) {
            for (int inCount = 0; inCount < count; inCount++) {
                if (intLine[inCount] > intLine[inCount + 1]) {
                    int cross = intLine[inCount];
                    intLine[inCount] = intLine[inCount + 1];
                    intLine[inCount + 1] = cross;
                }
            }
        }

        for (int x : intLine) {
            if (x != intLine[intLine.length - 1]) System.out.print(x + ",");
            else System.out.println(x);
        }
        return intLine;
    }

    public static int [] toDown (int[]intLine){
        System.out.println("По убыванию: ");
        for (int count = intLine.length - 1; count > 0; count--) {
            for (int inCount = 0; inCount < count; inCount++) {
                if (intLine[inCount] < intLine[inCount + 1]) {
                    int cross = intLine[inCount];
                    intLine[inCount] = intLine[inCount + 1];
                    intLine[inCount + 1] = cross;
                }
            }
        }

        for (int y : intLine) {
            if (y != intLine[intLine.length - 1]) System.out.print(y + ",");
            else System.out.print(y);
        }
        return intLine;
    }
}
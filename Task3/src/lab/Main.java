package lab;

public class Main {

    public static void main(String[] args) {
        System.out.println(fact(20));
    }

    public static long fact(int num) {
        if (num == 0) {
            return 1;
        } else {
            return fact(num - 1) * num;
        }
    }
}